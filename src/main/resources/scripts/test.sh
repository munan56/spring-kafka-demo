# 后台启动容器
docker-compose up -d

# 进入kafka容器
docker exec -it kafka_kafka_1 /bin/bash

# 创建 topic
  $KAFKA_HOME/bin/kafka-topics.sh --create --topic my_test_topic \
  --zookeeper kafka_zookeeper_1:2181 --replication-factor 1 \
  --partitions 1

# 查看topic
  $KAFKA_HOME/bin/kafka-topics.sh --zookeeper kafka_zookeeper_1:2181 \
  --describe --topic my_test_topic

# 测试发送消息
  $KAFKA_HOME/bin/kafka-console-producer.sh --topic=my_test_topic \
  --broker-list kafka_kafka_1:9092

#测试收取消息
  $KAFKA_HOME/bin/kafka-console-consumer.sh \
  --bootstrap-server kafka_kafka_1:9092 \
  --from-beginning --topic my_test_topic
