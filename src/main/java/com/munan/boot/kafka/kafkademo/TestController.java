package com.munan.boot.kafka.kafkademo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author munan
 * @version 1.0
 * @date 2020/4/20 16:27
 */
@RestController
@RequestMapping("/api")
public class TestController {

    @Autowired
    private KafkaProducer producer;


    @GetMapping("/send")
    public Boolean send(){
        producer.send("TEST-TOPIC","e");
        return Boolean.TRUE;
    }


}
