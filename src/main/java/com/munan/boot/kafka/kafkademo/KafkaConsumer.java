package com.munan.boot.kafka.kafkademo;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author munan
 * @version 1.0
 * @date 2020/4/23 9:40
 */

@Slf4j
@Component
public class KafkaConsumer {


    @KafkaListener(topics = {"TEST-TOPIC"},containerFactory = "kafkaListenerContainerFactory")
    public void listen(ConsumerRecord<?, ?> record, Consumer consumer) {
        log.info("kafka的key: " + record.key());
        log.info("kafka的value: " + record.value().toString());
        log.info("kafka topic"+record.topic());
        // 消费下一条
        consumer.commitAsync();

    }
}
