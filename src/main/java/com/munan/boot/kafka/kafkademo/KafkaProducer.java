package com.munan.boot.kafka.kafkademo;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.ProducerListener;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

/**
 * @author munan
 * @version 1.0
 * @date 2020/4/20 17:14
 */
@Slf4j
@Component
public class KafkaProducer  implements ProducerListener {

    @Autowired
    private KafkaTemplate kafkaTemplate;


    public Boolean send(String topic, Object data){
       kafkaTemplate.send(topic, data);
        return Boolean.TRUE;
    }

}
